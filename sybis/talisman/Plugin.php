<?php namespace Sybis\Talisman;

use Backend;
use System\Classes\PluginBase;

/**
 * Talisman Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Talisman',
            'description' => 'No description provided yet...',
            'author'      => 'Sybis',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Sybis\Talisman\Components\ListMenu' => 'listmenu',
            'Sybis\Talisman\Components\CommentList' => 'commentList',
            'Sybis\Talisman\Components\LanguageSwitcher' => 'languageSwitcher',
            'Sybis\Talisman\Components\RequestToTest' => 'requestToTest',
            'Sybis\Talisman\Components\TeachersList' => 'teacherList',
            'Sybis\Talisman\Components\WhyTalisman' => 'whyTalisman',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'sybis.talisman.some_permission' => [
                'tab' => 'Talisman',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'talisman' => [
                'label'       => 'listMenu',
                'url'         => Backend::url('sybis/talisman/address'),
                'icon'        => 'icon-leaf',
                'permissions' => ['sybis.talisman.*'],
                'order'       => 500,
                'sideMenu' => [
                    'address' => [
                        'label'       => 'Адреса',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('sybis/talisman/address'),
                    ],
                    'news' => [
                        'label'       => 'Новости',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('sybis/talisman/news'),
                    ],
                    'articles' => [
                        'label'       => 'Статьи',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('sybis/talisman/articles'),
                    ],
                    'certificates' => [
                        'label'       => 'Сертификаты',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('sybis/talisman/certificates'),
                    ],
                    'comments' => [
                        'label'       => 'Отзывы',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('sybis/talisman/comments'),
                    ],
                    'curriculums' => [
                        'label'       => 'Курсы',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('sybis/talisman/curriculums'),
                    ],
                    'employees' => [
                        'label'       => 'Сотрудники',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('sybis/talisman/employees'),
                    ],
                    'materials' => [
                        'label'       => 'Материалы',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('sybis/talisman/materials'),
                    ],
                    'requisites' => [
                        'label'       => 'Реквизиты',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('sybis/talisman/requisites'),
                    ],
                    'vacancies' => [
                        'label'       => 'Вакансии',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('sybis/talisman/vacancies'),
                    ],
                    'cities' => [
                        'label'       => 'Города',
                        'icon'        => 'icon-building-o',
                        'url'         => \Backend::url('sybis/talisman/cities'),
                    ],
                    'languages' => [
                        'label'       => 'Языки',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('sybis/talisman/languages'),
                    ],
                    'categories' => [
                        'label'       => 'Категории',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('sybis/talisman/categories'),
                    ],
                    'targetgroups' => [
                        'label'       => 'Целевые группы',
                        'icon'        => 'icon-list-alt',
                        'url'         => \Backend::url('sybis/talisman/targetgroups'),
                    ],
                ]
            ],
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'functions' => [
                'build_url' => function($pattern, $options) {
                    foreach ($options as $what => $with) {
                        $pattern = str_replace(":" . $what, $with, $pattern);
                    }

                    return $pattern;
                },

                'array_merge' => function() {
                    return call_user_func_array("array_merge", func_get_args());
                }
            ]
        ];
    }
}
