<?php namespace Sybis\Talisman\Models;

use Model;

/**
 * address Model
 */
class Address extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'address' => 'required|string|max:256',
        'phone'  => 'string|between:7,16',
        'city' => 'integer'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'sybis_talisman_address';
    
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = ['city' => ['Sybis\Talisman\Models\Cities']];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getCityOptions() {
        $cities = Cities::all();
        $result = [];
        foreach ($cities as $city) {
            $result[$city->id] = $city->title;
        }
        return $result;
    }

}