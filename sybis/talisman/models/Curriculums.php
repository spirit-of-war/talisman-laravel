<?php namespace Sybis\Talisman\Models;

use Model;

/**
 * curriculums Model
 */
class Curriculums extends Model
{

    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'title' => 'required|string|max:256',
//        'img'  => 'image|size:4000',
        'text' => 'string',
        'age_category_id' => 'integer'
    ];
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'sybis_talisman_curriculums';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'age_category' => ['Sybis\Talisman\Models\Categories'],
        'target_group' => 'Sybis\Talisman\Models\TargetGroups',
        'language' => 'Sybis\Talisman\Models\Languages',
    ];
    public $belongsToMany = [
        'cities' => ['Sybis\Talisman\Models\Cities', 'table' => 'sybis_talisman_cities_curriculums'],
        'employees' => ['Sybis\Talisman\Models\Employees', 'table' => 'sybis_talisman_employees_curriculums'],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = ['img' => ['System\Models\File']];
    public $attachMany = [];

}