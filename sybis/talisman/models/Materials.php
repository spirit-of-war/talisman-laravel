<?php namespace Sybis\Talisman\Models;

use Model;

/**
 * materials Model
 */
class Materials extends Model
{

    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'title' => 'required|string|max:256',
        'text'  => 'string',
//        'img' => 'image|size:4000'
    ];
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'sybis_talisman_materials';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = ['img' => ['System\Models\File']];
    public $attachMany = [];

}