<?php namespace Sybis\Talisman\Models;

use Model;

/**
 * requisites Model
 */
class Requisites extends Model
{

    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'title' => 'required|string|max:256',
        'city'  => 'integer|nullable',
        'address' => 'string|max:256',
        'r_s' => 'string|max:256',
        'bank_name' => 'string|max:256',
        'corr_account' => 'string|max:256',
        'bik' => 'string|max:256',
        'inn' => 'string|max:256',
        'kpp' => 'string|max:256',
        'ogrn' => 'string|max:256',
        'director' => 'string|max:256',
    ];
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'sybis_talisman_requisites';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = ['city' => ['Sybis\Talisman\Models\Cities']];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getCityOptions() {
        $cities = Cities::all();
        $result = [];
        foreach ($cities as $city) {
            $result[$city->id] = $city->title;
        }

        return $result;
    }
}