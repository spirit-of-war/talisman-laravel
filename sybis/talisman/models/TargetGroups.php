<?php namespace Sybis\Talisman\Models;

use Model;

/**
 * TargetGroups Model
 */
class TargetGroups extends Model
{

    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'title' => 'required|string|max:256',
        'slug'  => 'required|string|max:256',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'sybis_talisman_target_groups';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = ['curriculums' => ['Sybis\Talisman\Models\Curriculums', 'key' => 'target_group_id']];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}