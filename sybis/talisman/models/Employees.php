<?php namespace Sybis\Talisman\Models;

use Model;

/**
 * employees Model
 */
class Employees extends Model
{

    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'fio' => 'required|string|max:256',
        'description'  => 'string',
    ];
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'sybis_talisman_employees';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = ['curriculums' => ['Sybis\Talisman\Models\Curriculums', 'table' => 'sybis_talisman_employees_curriculums']];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = ['img' => ['System\Models\File']];
    public $attachMany = [];

    public function getCurriculumsListAttribute(){
        $curriculums = $this->curriculums()->get();
        $result = '';
        if(count($curriculums)){
            foreach($curriculums as $curriculum){
                $result[] = $curriculum->title;
            }
            $result = implode(', ', $result);
        }
        return $result;

    }

    public function getImageAttribute(){

        if($this->img()->first()) {
//            return '<img src="' . $this->img()->first()->getPath() . '" />';
            return $this->img()->first()->getPath();
        } else {
            return '';
        }

    }

}