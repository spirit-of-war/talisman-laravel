<?php namespace Sybis\Talisman\Models;

use Model;

/**
 * cities Model
 */
class Cities extends Model
{

    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'title' => 'required|string|max:256',
        'slug'  => 'required|string|max:256',
    ];
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'sybis_talisman_cities';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = ['curriculums' => ['Sybis\Talisman\Models\Curriculums', 'table' => 'sybis_talisman_cities_curriculums']];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}