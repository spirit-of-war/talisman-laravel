<?php namespace Sybis\Talisman\Models;

use Model;

/**
 * comments Model
 */
class Comments extends Model
{

    use \October\Rain\Database\Traits\Validation;
    
    public $rules = [
        'fio' => 'required|string|max:256',
//        'img'  => 'image|size:4000',
        'description' => 'string',
        'video' => 'string|max:256',
        'order' => 'integer',
        'text' => 'required|string',
        'is_view_video' => 'boolean',
        'target_group_id' => 'integer'
    ];
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'sybis_talisman_comments';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = ['target_group' => ['Sybis\Talisman\Models\TargetGroups']];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = ['img' => ['System\Models\File']];
    public $attachMany = [];

    public function getImageAttribute(){

            if($this->img()->first()) {
                return '<img src="' . $this->img()->first()->getPath() . '" />';
            } else {
                return '';
            }

    }

    public function getIsViewAttribute(){

        if($this->is_view_video) {
            return 'Да';
        } else {
            return 'Нет';
        }

    }

    public function getTargetGroupIdOptions() {
        $cities = TargetGroups::all();
        $result = [];
        foreach ($cities as $city) {
            $result[$city->id] = $city->title;
        }
        return $result;
    }

}