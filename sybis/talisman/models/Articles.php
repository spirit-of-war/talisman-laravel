<?php namespace Sybis\Talisman\Models;

use Model;

/**
 * articles Model
 */
class Articles extends Model
{

    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'title' => 'required|string|max:256',
        'text'  => 'string',
        'preview_text' => 'string'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'sybis_talisman_articles';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}