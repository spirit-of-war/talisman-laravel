<?php namespace Sybis\Talisman\Components;

use Cms\Classes\ComponentBase;
use Sybis\Talisman\Models\Employees;
use Sybis\Talisman\Models\Curriculums;

class TeachersList extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Преподаватели',
            'description' => 'Список преподавателей'
        ];
    }

    public function defineProperties()
    {
        return [
            'template' => [
                'title' => 'Шаблон',
                'description' => 'Выберите вид отображения шаблона',
                'type' => 'dropdown'
            ]
        ];
    }

    public function getTemplateOptions()
    {
        $result = [
            'all-employees' => 'Все сотрудники',
            'for-curriculums' => 'Для курса',
        ];

        return $result;
    }

    public function onRender()
    {
        $this->page['template'] = $this->property('template');
        $this->page['teachers'] = Employees::all();
    }

}