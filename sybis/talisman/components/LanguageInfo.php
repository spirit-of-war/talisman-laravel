<?php namespace Sybis\Talisman\Components;

use Cms\Classes\ComponentBase;

class LanguageInfo extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'LanguageInfo Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'language' => [
                 'title' => 'Language',
                 'description' => 'lang'
            ]
        ];
    }

    public function onRender() {
        $this->page['language'] = $this->property('language', 'test');
    }

}