<?php namespace Sybis\Talisman\Components;

use Cms\Classes\ComponentBase;
use Sybis\Talisman\classes\Helper;
use Sybis\Talisman\Models\Languages;
use Sybis\Talisman\Models\Cities;
use Sybis\Talisman\Models\TargetGroups;
use Sybis\Talisman\Models\Curriculums;

class LanguageSwitcher extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'LanguageSwitcher',
            'description' => 'Переключение языковых курсов'
        ];
    }

    public function defineProperties()
    {
        return [
            'language_slug_parameter' => [
                 'title' => 'Language Slug Parameter',
                 'description' => 'Именованный параметр страницы, из которого получаем символьный код выбранного языка',
                 'type' => 'string',
                 'required' => true,
                 'validationMessage' => 'Невозможно отобразить переключатель языка без параметра language_slug_parameter'
            ],

            'language_link_pattern' => [
                 'title' => 'Language Link Pattern',
                 'required' => true,
                 'description' => 'Шаблон, по которому стоятся ссылки на страницы для других языков'
            ],

            'target_group_slug_parameter' => [
                 'title' => 'Target Group Slug Parameter',
                 'description' => 'Именованный параметр страницы, из которого получаем символьный код текущей целевой группы'
            ]
        ];
    }

    public function onRender() {
        $language_slug_parameter = $this->property('language_slug_parameter');
        $language_slug = $this->param($language_slug_parameter);

        $this->page['current_language'] = Helper::getCurrentLanguage($language_slug);
        $this->page['current_city'] = Helper::getCurrentCity();

        
        $language_link_options = [];

        $curriculums = Curriculums::whereHas('cities', function($query) {
            $query->where('id', $this->page['current_city']->id);
        });
        
        $target_group_slug_parameter = $this->property('target_group_slug_parameter');

        if ($target_group_slug_parameter) {
            $target_group_slug = $this->param($target_group_slug_parameter);

            $this->page['current_target_group'] = TargetGroups::where('slug', $target_group_slug)->firstOrFail();

            $language_link_options['target_group_slug'] = $this->page['current_target_group']->slug;

            $curriculums = $curriculums->whereHas('target_group', function($query) {
                $query->where('id', $this->page['current_target_group']->id);
            });
        }

        $this->page['curriculums_ids'] = $curriculums->lists('id');

        $this->page['languages'] = Languages::whereHas('curriculums', function($query) {
            $query->whereIn('id', $this->page['curriculums_ids']);
        })->get();
        
        $language_found = false;

        foreach ($this->page['languages'] as $language) {
            if ($language->id === $this->page['current_language']->id) {
                $language_found = true;
                break;
            }
        }

        if (!$language_found) {
            // TODO: return normal 404 page here
            $this->setStatusCode(404);
            return $this->controller->run('404');
        }

        $this->page['language_link_pattern'] = $this->property('language_link_pattern');
        $this->page['language_link_options'] = $language_link_options;
    }

}