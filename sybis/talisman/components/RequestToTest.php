<?php namespace Sybis\Talisman\Components;

use Cms\Classes\ComponentBase;

class RequestToTest extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Отправить заявку на тест',
            'description' => 'Блок в котором пользователю предлагают отправить заявку на тестирование языка'
        ];
    }

    public function defineProperties()
    {
        return [
            'template' => [
                'title' => 'Шаблон',
                'description' => 'Выберите вид отображения шаблона',
                'type' => 'dropdown'
            ]
        ];
    }

    public function getTemplateOptions()
    {
        $result = [
            'primary-big' => 'Основной большой',
            'little-image' => 'Малый с картинкой',
            'text-white' => 'Текстовый белый (только для курса)',
        ];

        return $result;
    }

    public function onRender()
    {
        $this->page['template'] = $this->property('template');
    }

}