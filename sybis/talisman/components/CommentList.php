<?php namespace Sybis\Talisman\Components;

use Cms\Classes\ComponentBase;
use Sybis\Talisman\Models\Comments;
use Sybis\Talisman\Models\TargetGroups;

class CommentList extends ComponentBase
{

    public $maxItems;
    public function componentDetails()
    {
        return [
            'name'        => 'Отзывы',
            'description' => 'Выводит список отзывов'
        ];
    }

    public function defineProperties()
    {
        return [
            'maxItems' => [
                'title' => 'Количество отзывов',
                'description' => 'Отображать кнопку которая отобразит все отзывы',
                'default'           => 6,
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'Количество отзывов может содержать только цифры'
            ],
            'viewAllLink' => [
                'title' => 'Показывать Далее',
                'description' => 'Отображать кнопку которая отобразит все отзывы',
                'type' => 'checkbox',
            ],
            'viewVideoComment' => [
                'title' => 'Отзывы с видео',
                'description' => 'Отображать отзывы с видео',
                'type' => 'checkbox',
            ]
        ];
    }

    public function onRun()
    {
        $this->name = 'commentsComponent';



    }
    public function onRender()
    {
        $maxItems = $this->property('maxItems');
        $comments = Comments::orderBy('order', 'asc')->take($maxItems)->get();
        $this->renderCommentsToPage($comments);
        $this->page['filters'] = TargetGroups::all();

    }

    private function getBreakedForColumnsComments($comments, $columNumb = 1){
        $result = [];
        foreach($comments as $k => $comment) {
            if($k % 2 != 1){
                $result[1][] = $comment;
            } else {
                $result[2][] = $comment;
            }
        }
        if(isset($result[$columNumb])){
            return $result[$columNumb];
        } else {
            return false;
        }

    }

    private function renderCommentsToPage($comments) {
        $this->page['view_video_comment'] = $this->property('viewVideoComment');

        if($left_comments = $this->getBreakedForColumnsComments($comments, 1))
        $this->page['left_comments'] = $left_comments;
        if($right_comments = $this->getBreakedForColumnsComments($comments, 2))
        $this->page['right_comments'] = $right_comments;
    }

    public function onFilterComments()
    {
        $maxItems = $this->property('maxItems');
        $type = input('type');
        if(isset($type) && $type != 'all'){
            $targetGroup = TargetGroups::where('slug', $type)->first();
            $comments = Comments::where('target_group_id', $targetGroup->id)->orderBy('order', 'asc')->take($maxItems)->get();
        } else {
            $comments = Comments::orderBy('order', 'asc')->take($maxItems)->get();
        }

        $this->renderCommentsToPage($comments);
        return ['#comments_cont' => $this->renderPartial('commentList::comments')];
    }
}