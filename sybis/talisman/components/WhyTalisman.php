<?php namespace Sybis\Talisman\Components;

use Cms\Classes\ComponentBase;
use Sybis\Talisman\Models\Cities;

class WhyTalisman extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Почему Талисман',
            'description' => 'Блок Почему Талисман'
        ];
    }

    public function defineProperties()
    {
        return [
            'template' => [
                'title' => 'Шаблон',
                'description' => 'Выберите вид отображения шаблона',
                'type' => 'dropdown'
            ]
        ];
    }

    public function getTemplateOptions()
    {
        $result = [
            'primary' => 'Основной',
            'for-curriculums' => 'Для курса',
        ];

        return $result;
    }

    public function onRender()
    {
        $this->page['template'] = $this->property('template');
        $this->page['cities'] = Cities::all();
    }

}