<?php namespace Sybis\Talisman\Components;

use Cms\Classes\ComponentBase;

class ListMenu extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'ListMenu Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

}