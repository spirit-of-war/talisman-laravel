<?php namespace Sybis\Talisman\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Materials Back-end Controller
 */
class Materials extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Sybis.Talisman', 'talisman', 'materials');
    }
}