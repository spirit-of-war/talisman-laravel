<?php namespace Sybis\Talisman\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Target Groups Back-end Controller
 */
class TargetGroups extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Sybis.Talisman', 'talisman', 'targetgroups');
    }
}