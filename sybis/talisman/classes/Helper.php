<?php namespace Sybis\Talisman\classes;

use Request;

use Sybis\Talisman\Models\Cities;
use Sybis\Talisman\Models\Languages;

class Helper {
    public static function getCurrentSubdomain() {
        $regexp = '/^([\w-]+)\.' . config('app.root_host') . '$/';

        if (preg_match($regexp, Request::getHost(), $match)) {
            if ($match[1] === 'www') {
                return null;
            } else {
                return $match[1];
            }
        } else {
            return null;
        }
    }

    public static function getCurrentCity() {
        $subdomain = self::getCurrentSubdomain();

        if ($subdomain) {
            return Cities::where('slug', '=', $subdomain)->firstOrFail();
        } else {
            return Cities::where('slug', '=', config('app.root_city'))->firstOrFail();
        }
    }

    public static function getCurrentLanguage($language_slug) {
        return Languages::where('slug', '=', $language_slug)->firstOrFail();
    }
};