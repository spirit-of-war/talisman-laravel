<?php namespace Sybis\Talisman\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequisitesTable extends Migration
{
    public function up()
    {
        Schema::create('sybis_talisman_requisites', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('address')->nullable();
            $table->string('r_s')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('corr_account')->nullable();
            $table->string('bik')->nullable();
            $table->string('inn')->nullable();
            $table->string('kpp')->nullable();
            $table->string('ogrn')->nullable();
            $table->string('director')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sybis_talisman_requisites');
    }
}
