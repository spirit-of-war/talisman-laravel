<?php namespace Sybis\Talisman\Updates;

use Schema;
use Db;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class SeedTargetGroupIdOfCurriculums extends Migration
{
    public function up()
    {
        $first_target_id = Db::table('sybis_talisman_target_groups')
          ->orderBy('id', 'asc')
          ->pluck('id');

        Db::table('sybis_talisman_curriculums')->update([
            'target_group_id' => $first_target_id
        ]);
    }

    public function down()
    {

    }
}
