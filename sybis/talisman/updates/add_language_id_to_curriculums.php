<?php namespace Sybis\Talisman\Updates;

use Schema;
use Db;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddLanguageIdToCurriculums extends Migration
{
    public function up()
    {
        Schema::table('sybis_talisman_curriculums', function(Blueprint $table) {
            $table->integer('language_id')
                  ->unsigned()
                  ->nullable();
        });

        $first_language_id = Db::table('sybis_talisman_languages')
          ->orderBy('id', 'asc')
          ->pluck('id');

        Db::table('sybis_talisman_curriculums')->update([
            'language_id' => $first_language_id
        ]);

        Schema::table('sybis_talisman_curriculums', function(Blueprint $table) {
            $table->integer('language_id')
                  ->unsigned()
                  ->nullable(false)
                  ->change();

            $table->foreign('language_id')
                ->references('id')
                ->on('sybis_talisman_languages')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('sybis_talisman_curriculums', function(Blueprint $table) {
            $table->dropForeign('sybis_talisman_curriculums_language_id_foreign');
            $table->dropColumn('language_id');
        });
    }
}
