<?php namespace Sybis\Talisman\Updates;

use Schema;
use Db;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddTargetGroupIdToCurriculums extends Migration
{
    public function up()
    {
        Schema::table('sybis_talisman_curriculums', function(Blueprint $table) {
            $table->integer('target_group_id')
                  ->unsigned()
                  ->nullable();
        });
    }

    public function down()
    {
        Schema::table('sybis_talisman_curriculums', function(Blueprint $table) {
            $table->dropColumn('target_group_id');
        });
    }
}
