<?php namespace Sybis\Talisman\Updates;

/////// !!!!!!!!!!!!!!!!!!!!!!!!!!!!  оставил этот файл себе для примера. не применять!

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddColumnsToCommentsTable extends Migration
{
    public function up()
    {
        Schema::table('sybis_talisman_comments', function(Blueprint $table) {
            $table->string('video')->nullable();
            $table->integer('order')->nullable();
            $table->boolean('view_video')->default(false);
        });
    }

    public function down()
    {
        if (Schema::hasColumn('sybis_talisman_comments', 'video')) {
            Schema::table('sybis_talisman_comments', function (Blueprint $table) {
                $table->dropColumn('video');
            });
        }
    }
}
