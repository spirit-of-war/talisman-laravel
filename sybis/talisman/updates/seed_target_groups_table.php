<?php namespace Sybis\Talisman\Updates;

use Seeder;
use Db;

class SeedTargetGroupsTable extends Seeder
{
    public function run()
    {
        Db::table('sybis_talisman_target_groups')->insert([
            'title' => 'Дети',
            'slug'  => 'kids',
        ]);

        Db::table('sybis_talisman_target_groups')->insert([
            'title' => 'Взрослые',
            'slug'  => 'adults',
        ]);

        Db::table('sybis_talisman_target_groups')->insert([
            'title' => 'Компании',
            'slug'  => 'companies',
        ]);
    }
}
