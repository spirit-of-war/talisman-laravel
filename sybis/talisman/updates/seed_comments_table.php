<?php namespace Sybis\Talisman\Updates;

use Seeder;
use Db;

class SeedCommentsTable extends Seeder
{
    public function run()
    {
        Db::table('sybis_talisman_comments')->insert([
            'fio' => 'Василий Иванович',
            'description'  => 'Ссылка на видео',
            'video'  => 'https://www.youtube.com/embed/-v5pTI68xks',
            'order'  => 1,
            'text'  => 'Немного текста для всех ребят',
            'is_view_video'  => 1,
        ]);

        Db::table('sybis_talisman_comments')->insert([
            'fio' => 'Иван Васильевич',
            'description'  => 'Школьник 9 лет',
            'video'  => 'https://www.youtube.com/embed/jyBefGJZ8uQ',
            'order'  => 2,
            'text'  => 'Очень понравилось и все такое',
            'is_view_video'  => 0,
        ]);

        Db::table('sybis_talisman_comments')->insert([
            'fio' => 'Елтышев Виктор Викторович',
            'description'  => 'Главный бухгалтер',
            'video'  => 'https://www.youtube.com/embed/jyBefGJZ8uQ',
            'order'  => 4,
            'text'  => 'Ну довольно таки неплохой курс, но хотелось бы больше конкретики на тему главнокомандующего вторым белорусским фронтом. ',
            'is_view_video'  => 1,
        ]);

        Db::table('sybis_talisman_comments')->insert([
            'fio' => 'И последний отзыв ахаха',
            'description'  => 'описание описание описание',
            'video'  => 'https://www.youtube.com/embed/jyBefGJZ8uQ',
            'order'  => 3,
            'text'  => 'Очень много текста, ну ОЧЕНЬ много текстаааа',
            'is_view_video'  => 0,
        ]);
    }
}
