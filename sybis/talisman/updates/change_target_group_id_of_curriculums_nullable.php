<?php namespace Sybis\Talisman\Updates;

use Schema;
use Db;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class ChangeTargetGroupIdOfCurriculumsNullable extends Migration
{
    public function up()
    {
        Schema::table('sybis_talisman_curriculums', function(Blueprint $table) {
            $table->integer('target_group_id')
                  ->unsigned()
                  ->nullable(false)
                  ->change();
        });

        Schema::table('sybis_talisman_curriculums', function(Blueprint $table) {
            $table->foreign('target_group_id')
                ->references('id')
                ->on('sybis_talisman_target_groups')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('sybis_talisman_curriculums', function(Blueprint $table) {
            $table->dropForeign('sybis_talisman_curriculums_target_group_id_foreign');
        });

        Schema::table('sybis_talisman_curriculums', function(Blueprint $table) {
            $table->integer('target_group_id')
                  ->unsigned()
                  ->nullable()
                  ->change();
        });
    }
}
