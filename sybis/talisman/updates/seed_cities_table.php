<?php namespace Sybis\Talisman\Updates;

use Seeder;
use Db;

class SeedCitiesTable extends Seeder
{
    public function run()
    {
        Db::table('sybis_talisman_cities')->insert([
            'title' => 'Екатеринбург',
            'slug'  => 'ekburg',
        ]);

        Db::table('sybis_talisman_cities')->insert([
            'title' => 'Москва',
            'slug'  => 'msk',
        ]);

        Db::table('sybis_talisman_cities')->insert([
            'title' => 'Тюмень',
            'slug'  => 'tyumen',
        ]);

        Db::table('sybis_talisman_cities')->insert([
            'title' => 'Первоуральск',
            'slug'  => 'pervouralsk',
        ]);

        Db::table('sybis_talisman_cities')->insert([
            'title' => 'Нижний Тагил',
            'slug'  => 'ntagil',
        ]);
    }
}
