<?php namespace Sybis\Talisman\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCommentsTable extends Migration
{
    public function up()
    {
        Schema::create('sybis_talisman_comments', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('fio')->nullable();
            $table->text('description')->nullable();
            $table->text('text')->nullable();
            $table->string('video')->nullable();
            $table->integer('order')->nullable();
            $table->boolean('is_view_video')->default(false);
            $table->integer('target_group_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sybis_talisman_comments');
    }
}
