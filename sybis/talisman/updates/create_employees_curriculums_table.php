<?php namespace Sybis\Talisman\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateEmployeesCurriculumsTable extends Migration
{
    public function up()
    {
        Schema::create('sybis_talisman_employees_curriculums', function(Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('employees_id')->unsigned();
            $table->integer('curriculums_id')->unsigned();

            $table->primary(['employees_id', 'curriculums_id'], 'sybis_talisman_employees_curriculums_pk');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sybis_talisman_employees_curriculums');
    }
}
