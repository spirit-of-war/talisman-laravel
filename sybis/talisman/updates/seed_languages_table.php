<?php namespace Sybis\Talisman\Updates;

use Seeder;
use Db;

class SeedLanguagesTable extends Seeder
{
    public function run()
    {
        Db::table('sybis_talisman_languages')->insert([
            'title' => 'Английский',
            'slug'  => 'english',
        ]);

        Db::table('sybis_talisman_languages')->insert([
            'title' => 'Немецкий',
            'slug'  => 'nemeckij',
        ]);

        Db::table('sybis_talisman_languages')->insert([
            'title' => 'Испанский',
            'slug'  => 'ispanskij',
        ]);

        Db::table('sybis_talisman_languages')->insert([
            'title' => 'Итальянский',
            'slug'  => 'italyanskiy',
        ]);

        Db::table('sybis_talisman_languages')->insert([
            'title' => 'Французский',
            'slug'  => 'francuzskiy',
        ]);

        Db::table('sybis_talisman_languages')->insert([
            'title' => 'Китайский',
            'slug'  => 'kitajskij',
        ]);
    }
}
